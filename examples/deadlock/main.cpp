#include <twist/rt/run.hpp>

#include <twist/ed/stdlike/mutex.hpp>
#include <twist/ed/stdlike/thread.hpp>

int main() {
  twist::rt::Run([] {
    using twist::ed::stdlike::mutex;
    using twist::ed::stdlike::thread;

    static const size_t kTries = 128;

    for (size_t i = 0; i < kTries; ++i) {
      mutex a;
      mutex b;

      thread t1([&]() {
        a.lock();
        b.lock();
        b.unlock();
        a.unlock();
      });

      thread t2([&]() {
        b.lock();
        a.lock();
        a.unlock();
        b.unlock();
      });

      t1.join();
      t2.join();
    }
  });

  return 0;
}
