#include <twist/rt/run.hpp>

#include <twist/ed/stdlike/chrono.hpp>
#include <twist/ed/stdlike/thread.hpp>

#include <fmt/core.h>
#include <fmt/chrono.h>

using namespace std::chrono_literals;

auto Now() {
  return twist::ed::stdlike::steady_clock::now();
}

int main() {
  twist::rt::Run([] {
    auto start_time = Now();

    // Time is compressed for fibers
    twist::ed::stdlike::this_thread::sleep_for(100'500s);

    fmt::println("Elapsed: {}", Now() - start_time);
  });

  return 0;
}
