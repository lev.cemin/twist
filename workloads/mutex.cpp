#include <twist/rt/run.hpp>

#include <twist/ed/stdlike/mutex.hpp>

#include <twist/test/race.hpp>
#include <twist/test/budget.hpp>

using namespace std::chrono_literals;

int main() {
  auto params = twist::rt::Params().TimeBudget(3s);

  twist::rt::Run(params, [] {
    twist::test::Race race;

    twist::ed::stdlike::mutex mutex_;

    for (size_t i = 0; i < 7; ++i) {
      race.Add([&] {
        for (twist::test::TimeBudget budget; budget.Withdraw(); ) {
          std::lock_guard guard(mutex_);
        }
      });
    }

    race.Run();
  });

  return 0;
}
