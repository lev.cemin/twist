#include <twist/ed/stdlike/atomic.hpp>

#include <twist/rt/run.hpp>

#include <fmt/core.h>

int main() {
  twist::rt::Run([]() {
    // Your code goes here
    twist::ed::stdlike::atomic<bool> flag{false};
    flag.store(true);
  });

  return 0;
}
