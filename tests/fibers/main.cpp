#include <wheels/test/framework.hpp>

#if defined(TWIST_FIBERS)

#include <twist/rt/layer/fiber/runtime/scheduler.hpp>

#include <twist/ed/stdlike/mutex.hpp>

#include <twist/rt/run.hpp>

#include <fmt/core.h>

TEST_SUITE(FaultyFibers) {
   SIMPLE_TEST(Mutex) {
     twist::rt::Run([] {
       auto* scheduler = twist::rt::fiber::Scheduler::Current();

       {
         twist::ed::stdlike::mutex mutex;

         for (size_t i = 0; i < 128; ++i) {
           std::lock_guard guard(mutex);
         }
       }

       fmt::println("Futex count: {}", scheduler->FutexCount());

       ASSERT_LT(scheduler->FutexCount(), 10);
     });
   }
}

#endif

RUN_ALL_TESTS()
