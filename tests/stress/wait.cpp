#include <twist/test/with/wheels/stress.hpp>

#include <twist/ed/stdlike/atomic.hpp>
#include <twist/ed/stdlike/thread.hpp>
#include <twist/ed/wait/sys.hpp>

TEST_SUITE(SystemWait) {
  TWIST_TEST_REPEAT(Works, 5s) {
    twist::ed::stdlike::atomic<uint32_t> flag{0};

    twist::ed::stdlike::thread t([&] {
      auto wake_key = twist::ed::PrepareWake(flag);
      flag.store(1);
      twist::ed::WakeOne(wake_key);
    });

    twist::ed::Wait(flag, /*old=*/0);

    t.join();
  }
}
