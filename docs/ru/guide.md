# Twist

Задача _Twist_ – быстро и воспроизводимо проявить ошибки многопоточной синхронизации.

## Техники

Две главные техники, которые использует _Twist_, – это:

- внедрение сбоев (_fault injection_) и
- детерминированная симуляция.

### Fault injection

В точках обращения потоков к примитивам синхронизации _Twist_ внедряет "сбои" – псевдо-случайные переключения контекста,
что увеличивает покрытие графа состояний теста.

### Детерминированная симуляция

Недетерминированные многопоточные исполнения моделируются в _Twist_ с помощью однопоточных кооперативных файберов, что позволяет
- Детерминированно воспроизводить недетерминированные по своей природе баги
- Рандомизировать очередь планировщика и очереди ожидания в мьютексах и кондварах (в дополнение к внедрению сбоев)
- Вместить больше симуляций в ограниченный бюджет времени стресс-теста

## Навигация

Клиентская часть библиотеки – директория `twist/ed/`, пространство имен `twist::ed`

- `stdlike/` – замена стандартным примитивам синхронизации
- `wait/` – механизмы ожидания
- `local/` – тред-локальные переменные

Среда исполнения – директория `twist/rt/`, пространство имен `twist::rt`

### Правила
- Если вы пишете многопоточный код, то вам нужен **только** `twist/ed/`. 
- Если вы пишете тест для многопоточного кода, то вам нужен еще и `twist/rt/run/`

## Правила использования

Чтобы _Twist_ мог корректно работать и ловить баги, пользователь должен соблюдать следующие правила:

### Примитивы

Вместо `std::thread`, `std::atomic`, `std::mutex`, `std::condition_variable` и других примитивов из [Thread support library](https://en.cppreference.com/w/cpp/thread) необходимо использовать одноименные примитивы из пространства имен `twist::ed::stdlike`.

Примитивы из _Twist_ повторяют интерфейсы и сохраняют поведение примитивов из стандартной библиотеки, так что вы можете пользоваться документацией https://en.cppreference.com/w/ и **не думать** про _Twist_.

Заголовочные файлы меняются по следующему принципу: `#include <atomic>` заменяется на `#include <twist/ed/stdlike/atomic.hpp>`

Доступные заголовки: [twist/ed/stdlike](/twist/ed/stdlike)

При этом можно использовать `std::lock_guard` и `std::unique_lock` (но только в связке с `twist::ed::stdlike::mutex`), это не примитивы синхронизации, а RAII для более безопасного использования мьютекса.

Использование примитивов из `std` приведет к неопределенному поведению в тестах, будьте внимательны!

### Планировщик

Заголовочный файл: `twist/ed/stdlike/thread.hpp`

#### `yield`

Вместо `std::this_thread::yield` нужно использовать `twist::ed::stdlike::this_thread::yield`.

А еще лучше использовать `twist::ed::SpinWait` из заголовочного файла `<twist/ed/wait/spin.hpp>`.

#### `sleep_for`

Вместо `std::this_thread::sleep_for` нужно использовать `twist::ed::stdlike::this_thread::sleep_for`.

### Ожидание

_Twist_ предоставляет два механизма ожидания:
- активный (`SpinWait`) и
- блокирующий (`Wait`).

#### `SpinWait`

Заголовочный файл: `<twist/ed/wait/spin.hpp>`

Предназначен для _адаптивного_ _активного ожидания_ (_busy waiting_) в спинлоках.

##### Пример

[examples/spin/main.cpp](/examples/spin/main.cpp)

```cpp
void SpinLock::Lock() {
  // Одноразовый!
  // Для каждого нового цикла ожидания в каждом потоке 
  // нужно заводить отдельный экземпляр SpinWait
  twist::ed::SpinWait spin_wait;
  while (locked_.exchange(true)) {
    // У SpinWait определен operator()
    spin_wait();  // Exponential backoff
  }
}
```

#### `Wait`

Заголовочный файл: `twist/ed/wait/sys.hpp`

Для блокирующего ожидания вместо методов `wait` / `notify` у `std::atomic` нужно использовать
свободные функции `twist::ed::Wait` / `twist::ed::PrepareWake` + `twist::ed::Wake{One,All}`

##### Пример

[examples/wait/main.cpp](/examples/wait/main.cpp)

```cpp
class OneShotEvent {
 public:
  void Wait() {
    twist::ed::Wait(fired_, /*old=*/0);
  }

  void Fire() {
    // _До_ записи в fired_
    auto wake_key = twist::ed::PrepareWake(fired_);

    fired_.store(1);
    twist::ed::WakeAll(wake_key);
  }

 private:
  // Wait работает только с atomic-ом для типа uint32_t
  twist::ed::stdlike::atomic<uint32_t> fired_{0};
};
```

`Wake` – двухфазный: 
- сначала c помощью `PrepareWake` нужно получить ключ, адресующий связанную с атомиком очередь ожидания,
- затем разбудить потоки из очереди, передав в `Wake{One,All}` подготовленный ключ.

Вызывать `PrepareWake` следует **до записи** в атомик.

### `thread_local`

#### Ptr

Заголовочный файл: `<twist/ed/local/ptr.hpp>`

Вместо `thread_local T*` нужно использовать `twist::ed::ThreadLocalPtr<T>`:

- Для каждого потока хранит собственный адрес
- Повторяет поведение указателя
- Инициализируется `nullptr`-ом

Пример: [examples/local/main.cpp](/examples/local/main.cpp)

Для оптимальной производительности лучше использовать макрос `TWISTED_THREAD_LOCAL_PTR(T, name)`:
- в сборке с файберами за этим макросом будет находиться `twist::ed::ThreadLocalPtr<T>`,
- в сборке с потоками – `thread_local T*`.

#### Var

Заголовочный файл: `<twist/ed/local/var.hpp>`

Вместо `thread_local T` нужно использовать `twist::ed::ThreadLocal<T>`


### Отступления от `std`

- `thread` конструируется только от лямбды
- `atomic` намеренно не поддерживает методы `wait` / `notify` в пользу более безопасных внешних функций ожидания / пробуждения (см. `Wait`)
- default constructor для `atomic` помечен как `deprecated`: предпочитайте явную инициализацию

## Пример

Так будет выглядеть простейший Test-and-Set спинлок, написанный по правилам _Twist_:

```cpp
// Вместо #include <atomic>
#include <twist/ed/stdlike/atomic.hpp>

#include <twist/ed/wait/spin.hpp>

class SpinLock {
 public:
  void Lock() {
    twist::ed::SpinWait spin_wait;
    while (locked_.exchange(true)) {
      spin_wait();
    }
  }
  
  void Unlock() {
    locked_.store(false);  // <-- Здесь работает fault injection:
                           // любая операция над примитивом синхронизации может привести к
                           // переключению исполнения на другой поток
  }
 private:
  // Вместо std::atomic<bool>
  // "Твистовый" atomic повторяет API std::atomic,
  // так что можно пользоваться стандартной документацией:
  // https://en.cppreference.com/w/cpp/atomic/atomic
  
  twist::ed::stdlike::atomic<bool> locked_{false};
};
```

## Запуск кода

```cpp
#include <twist/rt/run.hpp>

#include <twist/ed/stdlike/thread.hpp>

int main() {
  twist::rt::Run([] {
    twist::ed::stdlike::thread t([] {
      for (size_t i = 0; i < 7; ++i) {
        twist::ed::stdlike::this_thread::yield();
      }
    });
  
    t.join();
  });
  
  return 0;
}
```
