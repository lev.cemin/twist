#pragma once

/*
 * Drop-in replacement for std::condition_variable
 * https://en.cppreference.com/w/cpp/thread/condition_variable
 *
 * Contents:
 *   namespace twist::ed::stdlike
 *     class condition_variable
 */

#if defined(TWIST_FAULTY)

#include <twist/rt/layer/fault/stdlike/condvar.hpp>

namespace twist::ed::stdlike {

using condition_variable = rt::fault::FaultyCondVar;  // NOLINT

}  // namespace twist::ed::stdlike

#else

#include <condition_variable>

namespace twist::ed::stdlike {

using ::std::condition_variable;

}  // namespace twist::ed::stdlike

#endif
