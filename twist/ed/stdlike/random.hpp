#pragma once

/*
 * Drop-in replacement for std::random_device
 *
 * Contents:
 *   namespace twist::ed::stdlike
 *     class random_device
 */

#if defined(TWIST_FIBERS)

#include <twist/rt/layer/fiber/stdlike/random.hpp>

namespace twist::ed::stdlike {

using random_device = rt::fiber::RandomDevice;  // NOLINT

}  // namespace twist::ed::stdlike

#else

#include <random>

namespace twist::ed::stdlike {

using ::std::random_device;

}  // namespace twist::ed::stdlike

#endif
