#pragma once

/*
 * Drop-in replacement for std::mutex
 * https://en.cppreference.com/w/cpp/thread/mutex
 *
 * Contents:
 *   namespace twist::ed::stdlike
 *     class mutex
 */

#if defined(TWIST_FAULTY)

#include <twist/rt/layer/fault/stdlike/mutex.hpp>

namespace twist::ed::stdlike {

using mutex = rt::fault::FaultyMutex;  // NOLINT

}  // namespace twist::ed::stdlike

#else

#include <mutex>

namespace twist::ed::stdlike {

using ::std::mutex;

}  // namespace twist::ed::stdlike

#endif
