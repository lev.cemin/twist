#pragma once

/*
 * Drop-in replacement for std::atomic<T>
 * https://en.cppreference.com/w/cpp/atomic/atomic
 *
 * Contents:
 *   namespace twist::ed::stdlike
 *     class atomic<T>
 */

#if defined(TWIST_FAULTY)

#include <twist/rt/layer/fault/stdlike/atomic.hpp>

namespace twist::ed::stdlike {

template <typename T>
using atomic = rt::fault::FaultyAtomic<T>;  // NOLINT

}  // namespace twist::ed::stdlike

#else

#include <atomic>

namespace twist::ed::stdlike {

using ::std::atomic;

}  // namespace twist::ed::stdlike

#endif
