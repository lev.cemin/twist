#pragma once

#include <twist/rt/layer/strand/local/ptr.hpp>

namespace twist::ed {

// Usage: examples/local/main.cpp
using rt::strand::ThreadLocalPtr;

// + TWISTED_THREAD_LOCAL_PTR(T, name)

}  // namespace twist::ed
