#pragma once

#include <twist/rt/layer/strand/local/var.hpp>

namespace twist::ed {

using rt::strand::ThreadLocal;

}  // namespace twist::ed
