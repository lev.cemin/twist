#pragma once

#include <twist/rt/layer/strand/wait/spin.hpp>

namespace twist::ed {

// Usage: examples/spin/main.cpp

using rt::strand::SpinWait;
using rt::strand::CpuRelax;

}  // namespace twist::ed
