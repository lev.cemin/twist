#pragma once

#include <twist/ed/stdlike/atomic.hpp>
#include <twist/test/inject_fault.hpp>
#include <twist/test/assert.hpp>

namespace twist::test {

class Plate {
 public:
  void Access() {
    TWIST_ASSERT(!accessed_.exchange(true, std::memory_order::relaxed),
                 "Mutual exclusion violated");

    ::twist::test::InjectFault();

    // Non-atomic access, potential data race
    ++access_count_;

    TWIST_ASSERT(accessed_.exchange(false, std::memory_order::relaxed),
                 "Mutual exclusion violated");
  }

  size_t AccessCount() const {
    return access_count_;
  }

 private:
  twist::ed::stdlike::atomic<bool> accessed_{false};
  size_t access_count_{0};
};

}  // namespace twist::test
