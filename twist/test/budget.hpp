#pragma once

#include <twist/rt/run/env/this.hpp>

#include <wheels/core/stop_watch.hpp>

#include <algorithm>

namespace twist::test {

inline bool KeepRunning() {
  return rt::this_env->KeepRunning();
}

// Single-threaded!

class TimeBudget {
  using Millis = std::chrono::milliseconds;

 public:
  static constexpr Millis kBatchThreshold = Millis(10);

  bool Withdraw() {
    ++count_;
    if (count_ == batch_) {
      Adapt();
      return KeepRunning();  // Access environment
    } else {
      return true;
    }
  }

  size_t Batch() const {
    return batch_;
  }

 private:
  void Adapt() {
    auto elapsed = batch_timer_.Elapsed();

    if (elapsed * 2 < kBatchThreshold) {
      batch_ *= 2;
    } else {
      // Restart batch
      batch_timer_.Restart();
      count_ = 0;

      if (elapsed > kBatchThreshold * 2) {
        batch_ = std::max(batch_ / 2, (size_t)1);
      }
    }
  }

 private:
  wheels::StopWatch<> batch_timer_;
  size_t count_ = 0;
  size_t batch_ = 1;
};

}  // namespace twist::test
