#pragma once

#include <twist/rt/layer/strand/stdlike/thread.hpp>
#include <twist/test/affinity.hpp>

#include <function2/function2.hpp>

#include <vector>

namespace twist::test {

using ThreadRoutine = fu2::unique_function<void()>;

////////////////////////////////////////////////////////////////////////////////

namespace detail {
void RunThreadRoutine(ThreadRoutine routine);
}  // namespace detail

////////////////////////////////////////////////////////////////////////////////

class Executor {
 public:
  void Submit(ThreadRoutine routine);

  ~Executor() {
    Join();
  }

  void Join();

 private:
  std::vector<rt::strand::stdlike::thread> threads_;
  bool joined_{false};
};

////////////////////////////////////////////////////////////////////////////////

rt::strand::stdlike::thread RunThread(ThreadRoutine routine);

}  // namespace twist::test
