#pragma once

#include <twist/test/chrono.hpp>

#include <wheels/core/stop_watch.hpp>

namespace twist::test {

using StopWatch = wheels::StopWatch<test::SteadyClock>;

}  // namespace twist::test
