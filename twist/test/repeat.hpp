#pragma once

#include <twist/test/budget.hpp>

namespace twist::test {

class Repeat {
 public:
  bool Test() {
    if (budget_.Withdraw()) {
      NewIter();
      return true;
    } else {
      return false;
    }
  }

  size_t Iter() {
    return iter_;
  }

  void Finish();

 private:
  void NewIter();

 private:
  twist::test::TimeBudget budget_;
  size_t iter_ = 0;
};

}  // namespace twist::test
