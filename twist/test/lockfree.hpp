#pragma once

#include <twist/rt/layer/fault/adversary/adversary.hpp>

#include <twist/ed/stdlike/mutex.hpp>

namespace twist::test {

//////////////////////////////////////////////////////////////////////

template <typename T>
class ReportProgressFor {
  class Proxy {
   public:
    Proxy(T* object) : object_(object) {
    }

    T* operator->() {
      return object_;
    }

    ~Proxy() {
      rt::fault::GetAdversary()->ReportProgress();
    }

   private:
    T* object_;
  };

 public:
  Proxy operator->() {
    return {&object_};
  }

 private:
  T object_;
};

//////////////////////////////////////////////////////////////////////

inline void ReportProgress() {
  rt::fault::GetAdversary()->ReportProgress();
}

//////////////////////////////////////////////////////////////////////

struct EnablePark {
  EnablePark() {
    rt::fault::GetAdversary()->EnablePark();
  }

  ~EnablePark() {
    rt::fault::GetAdversary()->DisablePark();
  }
};

struct DisablePark {
  DisablePark() {
    rt::fault::GetAdversary()->DisablePark();
  }

  ~DisablePark() {
    rt::fault::GetAdversary()->EnablePark();
  }
};

}  // namespace twist::test
