#pragma once

#include <twist/rt/layer/strand/stdlike/thread.hpp>
#include <twist/rt/layer/strand/stdlike/chrono.hpp>

namespace twist::test {

using rt::strand::stdlike::this_thread::sleep_for;

using rt::strand::steady_clock;

}  // namespace twist::test
