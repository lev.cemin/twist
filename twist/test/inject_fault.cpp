#include <twist/test/inject_fault.hpp>

#if defined(TWIST_FAULTY)

#include <twist/rt/layer/fault/adversary/inject_fault.hpp>

namespace twist::test {

void InjectFault() {
  rt::fault::InjectFault();
}

bool FaultsEnabled() {
  return true;
}

}  // namespace twist::test

#else

namespace twist::test {

void InjectFault() {
  // Nop
}

bool FaultsEnabled() {
  return false;
}

}  // namespace twist::test

#endif
