#pragma once

#include <twist/test/executor.hpp>

#include <twist/rt/layer/strand/stdlike/mutex.hpp>
#include <twist/rt/layer/strand/stdlike/condvar.hpp>

namespace twist::test {

namespace detail {

class RaceLatch {
 public:
  void Wait() {
    std::unique_lock lock(mutex_);
    while (!released_) {
      released_cond_.wait(lock);
    }
  }

  void Release() {
    std::lock_guard guard(mutex_);
    released_ = true;
    released_cond_.notify_all();
  }

 private:
  bool released_{false};
  rt::strand::stdlike::mutex mutex_;
  rt::strand::stdlike::condition_variable released_cond_;
};

}  // namespace detail

class Race {
 public:
  // TODO: remove in favor of AddThread
  template <typename F>
  void Add(F f) {
    AddThread(std::move(f));
  }

  template <typename F>
  void AddThread(F f) {
    executor_.Submit([this, f = std::move(f)]() mutable {
      start_latch_.Wait();
      f();
    });
  }

  void Run() {
    // Start participants
    start_latch_.Release();
    // Join participants
    executor_.Join();
  }

  void Start() {
    Run();
  }

 private:
  detail::RaceLatch start_latch_;
  Executor executor_;
};

}  // namespace twist::test
