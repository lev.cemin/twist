#pragma once

#include <twist/rt/layer/strand/random/uint64.hpp>

namespace twist::rt::fault {

using strand::RandomUInt64;

}  // namespace twist::rt::fault
