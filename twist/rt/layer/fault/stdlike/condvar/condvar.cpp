#include <twist/rt/layer/fault/stdlike/condvar.hpp>

/////////////////////////////////////////////////////////////////////

#define FAULTY_CONDVAR_IMPL

#if defined(TWIST_FIBERS)
#include <twist/rt/layer/fault/stdlike/condvar/fiber.ipp>
#else
#include <twist/rt/layer/fault/stdlike/condvar/thread.ipp>
#endif

#undef FAULTY_CONDVAR_IMPL

/////////////////////////////////////////////////////////////////////

namespace twist::rt::fault {

FaultyCondVar::FaultyCondVar()
    : pimpl_(std::make_unique<FaultyCondVar::Impl>()) {
  AccessAdversary();
}

FaultyCondVar::~FaultyCondVar() {
}

void FaultyCondVar::wait(Lock& lock) {  // NOLINT
  pimpl_->Wait(lock);
}

void FaultyCondVar::notify_one() {  // NOLINT
  pimpl_->NotifyOne();
}

void FaultyCondVar::notify_all() {  // NOLINT
  pimpl_->NotifyAll();
}

}  // namespace twist::rt::fault
