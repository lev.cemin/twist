#include <twist/rt/layer/fault/stdlike/thread.hpp>

// TODO: move to fault
#include <twist/test/affinity.hpp>

#include <twist/rt/layer/fault/adversary/adversary.hpp>

namespace twist::rt {
namespace fault {

FaultyThread::FaultyThread(ThreadRoutine routine)
    : impl_([routine = std::move(routine)]() mutable {
        test::SetThreadAffinity();
        GetAdversary()->Enter();
        routine();
        GetAdversary()->Exit();
      }) {
}

// NOLINTNEXTLINE
void FaultyThread::join() {
  impl_.join();
}

}  // namespace fault
}  // namespace twist