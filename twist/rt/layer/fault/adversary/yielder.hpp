#pragma once

#include <twist/rt/layer/fault/adversary/random_event.hpp>

#include <twist/rt/layer/strand/stdlike/thread.hpp>
#include <twist/rt/layer/strand/stdlike/atomic.hpp>

namespace twist::rt {
namespace fault {

/////////////////////////////////////////////////////////////////////

class Yielder {
 public:
  Yielder(size_t freq) : yield_(freq) {
  }

  void Reset() {
    yield_count_ = 0;
  }

  void MaybeYield() {
    if (yield_.Test()) {
      twist::rt::strand::stdlike::this_thread::yield();
      yield_count_.fetch_add(1, std::memory_order::relaxed);
    }
  }

  size_t YieldCount() const {
    return yield_count_.load(std::memory_order::relaxed);
  }

 private:
  RandomEvent yield_;

  // statistics
  strand::stdlike::atomic<size_t> yield_count_{0};
};

}  // namespace fault
}  // namespace twist::rt
