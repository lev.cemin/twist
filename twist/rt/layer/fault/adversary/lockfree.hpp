#pragma once

#include <twist/rt/layer/fault/adversary/adversary.hpp>

namespace twist::rt {
namespace fault {

IAdversaryPtr CreateLockFreeAdversary();

}  // namespace fault
}  // namespace twist::rt
