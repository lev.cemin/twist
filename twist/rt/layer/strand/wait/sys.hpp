#pragma once

#if defined(TWIST_FIBERS)

// cooperative user-space fibers

#include <twist/rt/layer/fiber/wait/sys.hpp>

namespace twist::rt::strand {

using fiber::Wait;
using fiber::WaitTimed;

using fiber::WakeKey;
using fiber::PrepareWake;

using fiber::WakeOne;
using fiber::WakeAll;

}  // namespace twist::rt::strand

#else

// native threads

#include <twist/rt/layer/thread/wait/sys/wait.hpp>

namespace twist::rt::strand {

using thread::Wait;
using thread::WaitTimed;

using thread::WakeKey;
using thread::PrepareWake;

using thread::WakeOne;
using thread::WakeAll;

}  // namespace twist::rt::strand:

#endif
