#include <twist/rt/layer/strand/random/uint64.hpp>

#if defined(TWIST_FIBERS)

#include <twist/rt/layer/fiber/runtime/scheduler.hpp>

namespace twist::rt::strand {

// Deterministic randomness for fibers

uint64_t RandomUInt64() {
  return fiber::Scheduler::Current()->RandomUInt64();
}

}  // namespace twist::rt::strand

#else

#include <twist/rt/layer/thread/random.hpp>

namespace twist::rt::strand {

uint64_t RandomUInt64() {
  return thread::RandomUInt64();
}

}  // namespace twist::rt::strand

#endif
