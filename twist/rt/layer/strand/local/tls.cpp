#include <twist/rt/layer/strand/local/tls.hpp>

#include <twist/rt/layer/fiber/runtime/scheduler.hpp>

namespace twist::rt::strand {

extern const uintptr_t kSlotUninitialized = 1;

// NB: destroyed before TLSManager
static thread_local ManagedTLS tls;

TLS& TLSManager::AccessTLS() {
#if defined(TWIST_FIBERS)
  return fiber::Scheduler::Current()->RunningFiber()->Fls();
#else
  return tls.Access();
#endif
}

}  // namespace twist::rt::strand
