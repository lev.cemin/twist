#include <twist/rt/layer/strand/stdlike/thread.hpp>

namespace twist::rt::strand::stdlike {

#if defined(TWIST_FIBERS)

const thread_id kInvalidThreadId = -1;

#else

const thread_id kInvalidThreadId{};

#endif

}  // namespace twist::rt::strand::stdlike
