#include <twist/rt/layer/fiber/stdlike/chrono.hpp>

#include <twist/rt/layer/fiber/runtime/scheduler.hpp>

namespace twist::rt::fiber {

SteadyClock::time_point SteadyClock::now() {
  return Scheduler::Current()->Now();
}

}  // namespace twist::rt::fiber
