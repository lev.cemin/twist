#include <twist/rt/layer/fiber/stdlike/random.hpp>

#include <twist/rt/layer/fiber/runtime/scheduler.hpp>

namespace twist::rt::fiber {

RandomDevice::result_type RandomDevice::Next() {
  return Scheduler::Current()->RandomUInt64();
}

}  // namespace twist::rt::fiber
