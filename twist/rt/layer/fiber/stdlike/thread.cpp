#include <twist/rt/layer/fiber/stdlike/thread.hpp>

#include <twist/rt/layer/fiber/runtime/scheduler.hpp>

#include <wheels/core/assert.hpp>

namespace twist::rt::fiber {

static Fiber * const kDetached = nullptr;
static Fiber * const kCompleted = (Fiber*)1;

ThreadLike::ThreadLike(FiberRoutine routine) {
  fiber_ = Scheduler::Current()->Spawn(std::move(routine));
  fiber_->SetWatcher(this);

  ParkingLot()->Descr("thread::join");
}

ThreadLike::~ThreadLike() {
  WHEELS_VERIFY(IsDetached(), "Thread join or detach required");
}

ThreadLike::ThreadLike(ThreadLike&& that) {
  MoveFrom(std::move(that));
}

ThreadLike& ThreadLike::operator =(ThreadLike&& that) {
  Reset();
  MoveFrom(std::move(that));
  return *this;
}

void ThreadLike::Completed() noexcept {
  fiber_ = kCompleted;
  ParkingLot()->WakeOne();
}

bool ThreadLike::joinable() const {
  return !IsDetached();
}

void ThreadLike::detach() {
  WHEELS_VERIFY(!IsDetached(), "Thread has already been detached");

  // Running or Completed

  if (IsRunning()) {
    // Detach
    fiber_->SetWatcher(nullptr);
  } else {
    // Do nothing
  }

  // Move to Detached state
  fiber_ = kDetached;
}

void ThreadLike::join() {
  WHEELS_VERIFY(!IsDetached(), "Thread has been detached");

  // Running or Completed

  if (IsRunning()) {
    // Wait for completion
    ParkingLot()->Park();
  } else {
    // Do nothing
  }

  // Move to Detached state
  fiber_ = kDetached;
}

unsigned int ThreadLike::hardware_concurrency() noexcept {
  return 4;
}

bool ThreadLike::IsDetached() const {
  return fiber_ == kDetached;
}

bool ThreadLike::IsCompleted() const {
  return fiber_ == kCompleted;
}

bool ThreadLike::IsRunning() const {
  return !IsDetached() && !IsCompleted();
}

void ThreadLike::Reset() {
  if (IsRunning()) {
    fiber_->SetWatcher(nullptr);
  }

  fiber_ = kDetached;
}

void ThreadLike::MoveFrom(ThreadLike&& that) {
  fiber_ = std::exchange(that.fiber_, kDetached);

  if (IsRunning()) {
    fiber_->SetWatcher(this);
  }
}

WaitQueue* ThreadLike::ParkingLot() {
  return Scheduler::Current()->Futex((FutexKey)this);
}

}  // namespace twist::rt::fiber
