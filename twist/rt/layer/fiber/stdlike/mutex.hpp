#pragma once

#include <twist/rt/layer/fiber/runtime/wait_queue.hpp>

#include <wheels/core/assert.hpp>

// std::lock_guard, std::unique_lock
#include <mutex>

namespace twist::rt::fiber {

class Mutex {
 public:
  Mutex() {
    Waiters()->Descr("mutex::lock");
  }

  void Lock() {
    while (locked_) {
      ++waiters_;
      Waiters()->Park();
      --waiters_;
    }
    locked_ = true;
  }

  bool TryLock() {
    return !std::exchange(locked_, true);
  }

  void Unlock() {
    WHEELS_VERIFY(locked_, "Unlocking mutex that is not locked");
    locked_ = false;
    if (waiters_ > 0) {
      Waiters()->WakeOne();
    }
  }

  // std::mutex / Lockable

  void lock() {  // NOLINT
    Lock();
  }

  bool try_lock() {  // NOLINT
    return TryLock();
  }

  void unlock() {  // NOLINT
    Unlock();
  }

 private:
  WaitQueue* Waiters();

 private:
  bool locked_{false};
  size_t waiters_ = 0;
};

}  // namespace twist::rt::fiber
