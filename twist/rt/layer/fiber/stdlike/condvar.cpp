#include <twist/rt/layer/fiber/stdlike/condvar.hpp>

#include <twist/rt/layer/fiber/runtime/scheduler.hpp>

namespace twist::rt::fiber {

WaitQueue* CondVar::Waiters() {
  return Scheduler::Current()->Futex((FutexKey)this);
}

}  // namespace twist::rt::fiber
