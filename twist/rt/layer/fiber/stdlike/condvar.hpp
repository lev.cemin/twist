#pragma once

#include <twist/rt/layer/fiber/runtime/wait_queue.hpp>
#include <twist/rt/layer/fiber/stdlike/mutex.hpp>

// std::unique_lock
#include <mutex>

namespace twist::rt::fiber {

class CondVar {
 public:
  CondVar() {
    Waiters()->Descr("condition_variable::wait");
  }

  void Wait(Mutex& mutex) {
    mutex.Unlock();
    Waiters()->Park();
    mutex.Lock();
  }

  void NotifyOne() {
    Waiters()->WakeOne();
  }

  void NotifyAll() {
    Waiters()->WakeAll();
  }

  // std::condition_variable interface

  using Lock = std::unique_lock<Mutex>;

  void wait(Lock& lock) {  // NOLINT
    Wait(*lock.mutex());
  }

  template <typename Predicate>
  void wait(Lock& lock, Predicate predicate) {  // NOLINT
    while (!predicate()) {
      wait(lock);
    }
  }

  void notify_one() {  // NOLINT
    NotifyOne();
  }

  void notify_all() {  // NOLINT
    NotifyAll();
  }

 private:
  WaitQueue* Waiters();

 private:
  //
};

}  // namespace twist::rt::fiber
