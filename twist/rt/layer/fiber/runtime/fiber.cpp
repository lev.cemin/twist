#include <twist/rt/layer/fiber/runtime/fiber.hpp>

#include <twist/rt/layer/fiber/runtime/scheduler.hpp>
#include <twist/rt/layer/fiber/runtime/stacks.hpp>

#include <wheels/core/compiler.hpp>
#include <wheels/core/exception.hpp>

namespace twist::rt::fiber {

Fiber::Fiber(Scheduler* scheduler,
             FiberRoutine routine,
             sure::Stack stack,
             FiberId id)
    : scheduler_(scheduler),
      routine_(std::move(routine)),
      stack_(std::move(stack)),
      state_(FiberState::Starting),
      id_(id) {
  context_.Setup(stack_.MutView(), /*trampoline=*/this);
}

void Fiber::Run() noexcept {
  SetState(FiberState::Running);

  try {
    scheduler_->DebugStart(this);
    routine_();
  } catch (...) {
    WHEELS_PANIC("Uncaught exception in fiber "
                 << Id() << ": " << wheels::CurrentExceptionMessage());
  }

  scheduler_->Terminate();  // never returns

  WHEELS_UNREACHABLE();
}

void Fiber::Resume() {
  scheduler_->Resume(this);
}

}  // namespace twist::rt::fiber
