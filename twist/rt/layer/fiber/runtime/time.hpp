#pragma once

#include <wheels/core/noncopyable.hpp>

#include <chrono>

namespace twist::rt::fiber {

class VirtualTime : private wheels::NonCopyable {
 public:
  using rep = uint64_t;
  using period = std::nano;
  using duration = std::chrono::duration<rep, period>;
  using time_point = std::chrono::time_point<VirtualTime>;

 public:
  VirtualTime() {
    Reset();
  }

  void AdvanceBy(duration delta) {
    now_ += delta;
  }

  void AdvanceTo(time_point future);

  void Reset() {
    now_ = time_point{duration{0}};
  }

  time_point Now() const {
    return now_;
  }

  time_point After(std::chrono::milliseconds timeout) {
    return Now() + timeout;
  }

 private:
  time_point now_;
};

};  // namespace twist::rt::fiber
