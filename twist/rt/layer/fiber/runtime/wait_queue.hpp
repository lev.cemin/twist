#pragma once

#include <twist/rt/layer/fiber/runtime/queue.hpp>
#include <twist/rt/layer/fiber/runtime/time.hpp>

#include <string>

namespace twist::rt::fiber {

class WaitQueue {
 public:
  WaitQueue(std::string_view descr = "?");

  void Descr(std::string str);

  void Park();
  void ParkTimed(VirtualTime::time_point deadline);

  void WakeOne();
  void WakeAll();

 private:
  // For deadlock report
  std::string descr_;

  FiberQueue waiters_;
};

}  // namespace twist::rt::fiber
