#pragma once

#include <function2/function2.hpp>

namespace twist::rt::fiber {

using FiberRoutine = fu2::unique_function<void()>;

}  // namespace twist::rt::fiber
