#pragma once

#include <twist/rt/layer/fiber/runtime/fwd.hpp>
#include <twist/rt/layer/fiber/runtime/routine.hpp>
#include <twist/rt/layer/fiber/runtime/id.hpp>
#include <twist/rt/layer/fiber/runtime/watcher.hpp>
#include <twist/rt/layer/fiber/runtime/state.hpp>

#include <twist/rt/layer/strand/local/tls.hpp>

#include <sure/context.hpp>
#include <sure/stack.hpp>

#include <wheels/intrusive/list.hpp>

#include <string>

namespace twist::rt::fiber {

class Fiber : public wheels::IntrusiveListNode<Fiber>,
              private sure::ITrampoline {
  friend class Scheduler;

 public:
  FiberId Id() const {
    return id_;
  }

  sure::ExecutionContext& Context() {
    return context_;
  }

  sure::Stack& Stack() {
    return stack_;
  }

  FiberState State() const {
    return state_;
  }

  void SetState(FiberState target) {
    state_ = target;
  }

  std::string_view Where() const {
    return where_;
  }

  void SetWhere(std::string_view descr) {
    where_ = descr;
  }

  strand::TLS& Fls() {
    return fls_.Access();
  }

  IFiberWatcher* Watcher() const {
    return watcher_;
  }

  void SetWatcher(IFiberWatcher* watcher) {
    watcher_ = watcher;
  }

  void Resume();

  size_t StepCount() const {
    return steps_;
  }

 private:
  Fiber(Scheduler* scheduler,
        FiberRoutine routine,
        sure::Stack stack,
        FiberId id);

  // sure::ITrampoline
  void Run() noexcept override;

 private:
  Scheduler* scheduler_;
  FiberRoutine routine_;
  sure::Stack stack_;
  sure::ExecutionContext context_;
  FiberState state_;
  strand::ManagedTLS fls_;
  FiberId id_;
  std::string_view where_{"-"};
  IFiberWatcher* watcher_{nullptr};
  size_t steps_{0};
};

}  // namespace twist::rt::fiber
