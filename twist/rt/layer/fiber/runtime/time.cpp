#include <twist/rt/layer/fiber/runtime/time.hpp>

#include <wheels/core/assert.hpp>

namespace twist::rt::fiber {

void VirtualTime::AdvanceTo(time_point future) {
  WHEELS_VERIFY(future >= now_, "Time cannot move backward");
  now_ = future;
}

}  // namespace twist::rt::fiber
