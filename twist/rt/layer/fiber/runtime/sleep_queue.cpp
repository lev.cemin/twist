#include <twist/rt/layer/fiber/runtime/sleep_queue.hpp>

#include <wheels/core/assert.hpp>

namespace twist::rt::fiber {

void SleepQueue::Put(Fiber* sleeper, TimePoint deadline) {
  sleepers_.push({sleeper, deadline});
}

bool SleepQueue::IsEmpty() const {
  return sleepers_.empty();
}

Fiber* SleepQueue::Poll(TimePoint now) {
  if (IsEmpty()) {
    return nullptr;
  }

  Entry e = sleepers_.top();
  if (e.deadline <= now) {
    sleepers_.pop();
    return e.fiber;
  } else {
    return nullptr;
  }
}

SleepQueue::TimePoint SleepQueue::NextDeadLine() const {
  WHEELS_VERIFY(!IsEmpty(), "Sleep queue is empty");
  return sleepers_.top().deadline;
}

}  // namespace twist::rt::fiber
