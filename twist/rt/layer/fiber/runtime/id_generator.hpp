#pragma once

#include <twist/rt/layer/fiber/runtime/id.hpp>

namespace twist::rt::fiber {

class IdGenerator {
 public:
  FiberId Generate() {
    return ++last_id_;
  }

  void Reset() {
    last_id_ = 0;
  }

 private:
  size_t last_id_ = 0;
};

}  // namespace twist::rt::fiber
