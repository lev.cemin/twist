#pragma once

#include <twist/rt/layer/fiber/runtime/fiber.hpp>
#include <twist/rt/layer/fiber/runtime/queue.hpp>
#include <twist/rt/layer/fiber/runtime/time.hpp>
#include <twist/rt/layer/fiber/runtime/futex.hpp>
#include <twist/rt/layer/fiber/runtime/wait_queue.hpp>
#include <twist/rt/layer/fiber/runtime/sleep_queue.hpp>
#include <twist/rt/layer/fiber/runtime/id_generator.hpp>
#include <twist/rt/layer/fiber/runtime/random.hpp>
#include <twist/rt/layer/fiber/runtime/watcher.hpp>

#include <sure/context.hpp>

#include <chrono>
#include <map>
#include <set>
#include <string_view>
#include <random>

namespace twist::rt::fiber {

class Scheduler : private IFiberWatcher {
  friend class Fiber;

 public:
  Scheduler(size_t seed = 42);

  void Seed(size_t seed);

  // One-shot
  void Run(FiberRoutine main);

  static Scheduler* Current();

  // System calls

  Fiber* Spawn(FiberRoutine);
  void Yield();
  void SleepFor(std::chrono::milliseconds delay);
  void Suspend(std::string_view where);
  void Resume(Fiber* waiter);
  void Terminate();

  Fiber* RunningFiber() const;

  VirtualTime::time_point Now() const {
    return time_.Now();
  }

  uint64_t RandomUInt64();

  // Ignore upcoming Yield calls
  void PreemptDisable(bool disable = true) {
    preempt_disabled_ = disable;
  }

  // Statistics for tests
  size_t SwitchCount() const {
    return switch_count_;
  }

  WaitQueue* Futex(FutexKey key);

  // Statistics for tests
  size_t FutexCount() const;

  VirtualTime& Time() {
    return time_;
  }

 private:
  // IFiberWatcher
  void Completed() noexcept override;

  void BeforeStart();
  void RunLoop();
  void AfterStop();

  void CheckDeadlock();
  void ReportDeadlockAndDie();
  void ReportFromDeadlockedFiber(Fiber*);

  bool IsIdle() const;
  void PollSleepQueue();
  void TimeKeeper();

  // Advance virtual time
  void Tick();

  // Context switches

  // Scheduler -> fiber
  void SwitchTo(Fiber*);
  // Running fiber -> scheduler
  void SwitchToScheduler();

  void DebugResume(Fiber*);
  void DebugSwitch(Fiber*);
  void DebugStart(Fiber*);

  Fiber* PickReadyFiber();
  // Context switch: scheduler -> fiber
  void Step(Fiber*);
  // Handle system call in scheduler context
  void Dispatch(Fiber*);
  // Add fiber to run queue
  void Schedule(Fiber*);

  Fiber* CreateFiber(FiberRoutine);
  void Destroy(Fiber*);

  size_t AliveCount() const;

 private:
  sure::ExecutionContext loop_context_;  // Thread context

  FiberQueue run_queue_;
  Fiber* running_{nullptr};
  SleepQueue sleep_queue_;
  std::set<Fiber*> alive_;

  std::map<FutexKey, WaitQueue> futex_;

  VirtualTime time_;
  IdGenerator ids_;

  size_t seed_;
  RandomGenerator random_;

  bool preempt_disabled_{false};

  // Statistics
  size_t futex_count_{0};
  size_t switch_count_{0};
};

}  // namespace twist::rt::fiber
