#include <twist/rt/layer/fiber/runtime/syscalls.hpp>

#include <twist/rt/layer/fiber/runtime/scheduler.hpp>

namespace twist::rt::fiber {

void Spawn(FiberRoutine routine) {
  Scheduler::Current()->Spawn(std::move(routine));
}

void Yield() {
  Scheduler::Current()->Yield();
}

void SleepFor(std::chrono::milliseconds delay) {
  Scheduler::Current()->SleepFor(delay);
}

FiberId GetId() {
  return Scheduler::Current()->RunningFiber()->Id();
}

}  // namespace twist::rt::fiber
