#pragma once

#include <twist/rt/layer/fiber/runtime/syscalls.hpp>

namespace twist::rt::fiber {

class [[nodiscard]] SpinWait {
 public:
  void Spin() {
    Yield();
  }

  void operator()() {
    Spin();
  }
};

inline void CpuRelax() {
  Yield();
}

}  // namespace twist::rt::fiber
