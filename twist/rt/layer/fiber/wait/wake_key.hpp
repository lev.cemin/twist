#pragma once

#include <twist/rt/layer/fiber/runtime/futex.hpp>

namespace twist::rt::fiber {

struct WakeKey {
  FutexKey futex_key;
};

}  // namespace twist::rt::fiber
