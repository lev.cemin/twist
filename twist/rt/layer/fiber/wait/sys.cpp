#include <twist/rt/layer/fiber/wait/sys.hpp>

#include <twist/rt/layer/fiber/runtime/scheduler.hpp>

namespace twist::rt::fiber {

namespace {

FutexKey FutexKeyFor(Atomic<uint32_t>& atomic) {
  return (uint32_t*)&atomic;
}

WaitQueue* WaitQueueBy(FutexKey key) {
  return Scheduler::Current()->Futex(key);
}

}  // namespace

void Wait(Atomic<uint32_t>& atomic, uint32_t old, std::memory_order mo) {
  WaitQueue* wait_queue = WaitQueueBy(FutexKeyFor(atomic));
  while (atomic.load(mo) == old) {
    wait_queue->Park();
  }
}

bool WaitTimed(Atomic<uint32_t>& atomic, uint32_t old, std::chrono::milliseconds timeout) {
  Scheduler* scheduler = Scheduler::Current();

  WaitQueue* wait_queue = scheduler->Futex(FutexKeyFor(atomic));

  VirtualTime& time = scheduler->Time();
  auto deadline = time.After(timeout);

  while (atomic.load() == old) {
    if (time.Now() >= deadline) {
      return false;
    }
    wait_queue->ParkTimed(deadline);
  }

  return true;
}

WakeKey PrepareWake(Atomic<uint32_t>& atomic) {
  return {FutexKeyFor(atomic)};
}

void WakeOne(WakeKey key) {
  WaitQueueBy(key.futex_key)->WakeOne();
}

void WakeAll(WakeKey key) {
  WaitQueueBy(key.futex_key)->WakeAll();
}

}  // namespace twist::rt::fiber
