#include <twist/rt/layer/thread/wait/sys/platform/wait.hpp>

#include <ctime>

#if LINUX

#include <unistd.h>
#include <sys/syscall.h>
#include <linux/futex.h>

namespace {

// There is no glibc wrapper for 'futex' syscall

int futex(unsigned int* uaddr, int op, int val, const struct timespec* timeout,
          int* uaddr2, int val3) {
  return syscall(SYS_futex, uaddr, op, val, timeout, uaddr2, val3);
}

}  // namespace

namespace twist::rt {
namespace thread {

int PlatformWaitTimed(uint32_t* addr, uint32_t old, uint32_t millis) {
  struct timespec timeout;
  timeout.tv_sec = millis / 1000;
  timeout.tv_nsec = (millis % 1000) * 1000'000;

  return futex(addr, FUTEX_WAIT_PRIVATE, old, &timeout, nullptr, 0);
}

int PlatformWait(uint32_t* addr, uint32_t old) {
  return futex(addr, FUTEX_WAIT_PRIVATE, old, nullptr, nullptr, 0);
}

int PlatformWake(uint32_t* addr, size_t count) {
  return futex(addr, FUTEX_WAKE_PRIVATE, (int)count, nullptr, nullptr, 0);
}

}  // namespace thread
}  // namespace twist::rt

#elif APPLE

extern "C" int __ulock_wait(uint32_t operation, void *addr, uint64_t value,
                            uint32_t timeout); /* timeout is specified in microseconds */
extern "C" int __ulock_wake(uint32_t operation, void *addr, uint64_t wake_value);

#define UL_COMPARE_AND_WAIT				1
#define ULF_WAKE_ALL					0x00000100


namespace twist::rt {
namespace thread {

int PlatformWaitTimed(uint32_t* addr, uint32_t expected, uint32_t millis) {
  return __ulock_wait(UL_COMPARE_AND_WAIT, addr, expected, millis * 1000);
}

int PlatformWait(uint32_t* addr, uint32_t old) {
  return PlatformWaitTimed(addr, old, /*millis=*/0);
}

int PlatformWake(uint32_t* addr, size_t count) {
  return __ulock_wake(UL_COMPARE_AND_WAIT | ((count == 1) ? 0 : ULF_WAKE_ALL), addr, 0);
}

}  // namespace thread
}  // namespace twist::rt


#else

#pragma message("PlatformWait is not implemented")

// TODO: emulate futex with mutex+condvar

#endif
