#pragma once

#include <cstdint>

namespace twist::rt::thread {

struct WakeKey {
  uint32_t* addr;
};

}  // namespace twist::rt::thread
