#pragma once

#include <twist/single_core.hpp>

#if TWIST_SINGLE_CORE

#include <twist/rt/layer/thread/wait/spin/cores/single.hpp>

namespace twist::rt::thread {

using cores::single::SpinWait;

}  // namespace twist::rt::thread

#else

#include <twist/rt/layer/thread/wait/spin/cores/multi.hpp>

namespace twist::rt::thread {

using cores::multi::SpinWait;

}  // namespace twist::rt::thread

#endif

#include <twist/rt/layer/thread/wait/spin/relax.hpp>

namespace twist::rt::thread {

using cores::CpuRelax;

}  // namespace twist::rt::thread
