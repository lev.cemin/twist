#pragma once

#include <cstdint>

namespace twist::rt::thread {

uint64_t RandomUInt64();

}  // namespace twist::rt::thread
