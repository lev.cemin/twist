#include <twist/rt/run/runners/thread.hpp>

#include <twist/rt/run/env/this.hpp>

#include <twist/rt/layer/fault/adversary/adversary.hpp>

namespace twist::rt {

void ThreadRunner::Run(Routine main) {
  this_env = env_;

  main();

  this_env = nullptr;
}

void ThreadRunner::Finish() {
#if defined(TWIST_FAULTY)
  fault::GetAdversary()->PrintReport(env_);
#endif
}

}  // namespace twist::rt
