#include <twist/rt/run/runners/fiber.hpp>
#include <twist/rt/run/env/this.hpp>

#include <twist/rt/layer/fault/adversary/adversary.hpp>

#include <wheels/core/hash.hpp>

#include <fmt/core.h>

namespace twist::rt {

FiberRunner::FiberRunner(IEnv* env)
    : env_(env) {
  Prepare();
}

void FiberRunner::Prepare() {
  seed_ = env_->Seed();
  env_->PrintLine(fmt::format("Seed for deterministic simulation: {}", seed_));

#if defined(TWIST_FAULTY)
  fault::GetAdversary()->Reset();
#endif
}

void FiberRunner::Run(Routine main) {
  this_env = env_;

  scheduler_.Seed(seed_);

  scheduler_.Run(std::move(main));

  // Compute seed for the next run
  wheels::HashCombine(seed_, ++runs_);

  this_env = nullptr;
}

void FiberRunner::Finish() {
  if (runs_ > 1) {
    env_->PrintLine(fmt::format("Runs: {}", runs_));
  }
#if defined(TWIST_FAULTY)
  fault::GetAdversary()->PrintReport(env_);
#endif
}

}  // namespace twist::rt
