#pragma once

#include <twist/rt/run.hpp>
#include <twist/rt/run/env.hpp>

#include <twist/rt/layer/fiber/runtime/scheduler.hpp>

namespace twist::rt {

class ThreadRunner {
 public:
  ThreadRunner(IEnv* env)
      : env_(env) {
  }

  void Run(Routine main);
  void Finish();

 private:
  IEnv* env_;
};

}  // namespace twist::rt
