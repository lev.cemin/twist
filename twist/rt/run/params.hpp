#pragma once

#include <cstdlib>
#include <chrono>

namespace twist::rt {

struct Params {
  size_t seed = 42;
  std::chrono::seconds time_budget = std::chrono::seconds(1);

  Params& Seed(size_t value) {
    seed = value;
    return *this;
  }

  Params& TimeBudget(std::chrono::seconds secs) {
    time_budget = secs;
    return *this;
  }
};

}  // namespace twist::rt
