#pragma once

#include <twist/rt/run/params.hpp>
#include <twist/rt/run/env.hpp>

#include <function2/function2.hpp>

namespace twist::rt {

using Routine = fu2::unique_function<void()>;

void Run(IEnv* env, Routine main);

void Run(Routine main);
void Run(Params, Routine main);

}  // namespace twist::rt
